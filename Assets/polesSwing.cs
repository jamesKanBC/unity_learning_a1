﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class polesSwing : MonoBehaviour {

    public GameObject robot;
    public LineRenderer line; 
    private DistanceJoint2D Swing;

    bool clickedOn;

	// Use this for initialization
	void Start () {
        Swing = GetComponent<DistanceJoint2D>();
        Swing.enabled = false;
        line.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (clickedOn)
        {
            Swing.enabled = true;
            line.enabled = true;
            line.SetPosition(0,transform.position);
            line.SetPosition(1, robot.transform.position);
        }
        else {
            Swing.enabled = false;
            line.enabled = false;
        }
	}
    
    void OnMouseDown()
    {
        clickedOn = true;
    }

    private void OnMouseUp()
    {
        clickedOn = false;
    }
}
