﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : MonoBehaviour
{

    public GameObject robot;
    public DistanceJoint2D Swing;
    bool clickedOn;


    // Use this for initialization
    void Start()
    {
        Swing = GetComponent<DistanceJoint2D>();
        Swing.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (clickedOn)
        {
            Swing.enabled = true;
        }
        else
        {
            Swing.enabled = false;
        }
    }

    void OnMouseDown()
    {
        clickedOn = true;
    }

    private void OnMouseUp()
    {
        clickedOn = false;
    }
}
