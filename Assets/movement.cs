﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {

    // Use this for initialization
    private Rigidbody2D robot;
    public float speed = 10f;
    public float jumpSpeed = 20000f;

    // Line Renderer
    public LineRenderer rope;

    //Handling grounding
    public Transform groundCheckPoint;
    public float groundCheckRadius;
    public LayerMask groundLayer;
    public bool isGrounded;

	void Start () {
		robot = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, groundLayer);
        if (Input.GetKeyDown(KeyCode.W) && isGrounded) {
           
            Debug.Log(Vector2.up * jumpSpeed);
            robot.AddForce(Vector2.up * jumpSpeed);
        }
        //Issue here, using addfoce and not specifying which direction causes the continuous addition of y axis
        // robot.AddForce (movement* speed,ForceMode2D.Force);

        if(isGrounded)
        robot.velocity = new Vector2(moveHorizontal * speed, robot.velocity.y);
    }

    public void Update()
    {
      
    }
    }

